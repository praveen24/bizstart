<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['about-us'] = 'home/about';
$route['contact'] = 'home/contact';
$route['uae-mainland-business-setup'] = 'home/mainland';
$route['business-setup-in-uae-freezone'] = 'home/freezone';
$route['offshore-business-setup-in-dubai'] = 'home/offshore';
$route['contact_info'] = 'home/contact_info';
$route['commercial-license'] = 'home/commercial_license';
$route['business-centers-in-uae'] = 'home/business_centers';
$route['refer-and-earn'] = 'home/refer';
$route['get_appoinment'] = 'home/get_appoinment';
$route['signup'] = 'home/signup';
$route['added-service'] = 'home/added_service';
$route['business-setup-in-dubai'] = 'home/dubai_business';
$route['business-setup-in-sharjah'] = 'home/sharjah_business';
$route['business-setup-in-fujairah'] = 'home/fujeirah_business';
$route['business-setup-in-ajman'] = 'home/ajman_business';
$route['business-setup-in-ras-al-khaimah'] = 'home/ras_al_khaima_business';
$route['business-setup-in-abudhabi'] = 'home/abudhabi_business';
$route['business-setup-in-ded'] = 'home/ded_business';
$route['business-setup-in-dwc'] = 'home/dwc_business';
$route['business-setup-in-dmcc'] = 'home/dmcc_business';
$route['business-setup-in-saif-zone'] = 'home/saif_business';
$route['business-setup-in-jabel-ali'] = 'home/jabel_business';
$route['business-setup-in-silicon-oasis'] = 'home/silicon_business';
$route['flexi-desk-in-uae'] = 'home/flexi_desk';
$route['office-for-rent-in-uae'] = 'home/rental_office';
$route['meeting-rooms-in-uae'] = 'home/meeting_room';
$route['co-working-space-in-uae'] = 'home/co_working';