<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('home_model');
    }

    public function index()
    {
        $data['data'] = $this->data;
        $data['view_page'] = 'home/index';
        $this->load->view('home/template', $data);
    }

    public function about()
    {
        $data['data'] = $this->data;
        $data['view_page'] = 'home/about';
        $this->load->view('home/template', $data);
    }

    public function mainland()
    {
        $data['data'] = $this->data;
        $data['view_page'] = 'home/mainland';
        $this->load->view('home/template', $data);
    }

    public function freezone()
    {
        $data['data'] = $this->data;
        $data['view_page'] = 'home/freezone';
        $this->load->view('home/template', $data);
    }

    public function offshore()
    {
        $data['data'] = $this->data;
        $data['view_page'] = 'home/offshore';
        $this->load->view('home/template', $data);
    }

    public function contact()
    {
        $data['data'] = $this->data;
        $data['view_page'] = 'home/contact';
        $this->load->view('home/template', $data);
    }

    public function contact_info(){
        $name = $this->input->post('name');  
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $message = $this->input->post('message');
        $data = array('name' => $name, 'email' =>$email, 'phone' => $phone, 'message' => $message);
        $mail_body = $this->load->view('mail/contact_form', $data, TRUE);

        $this->load->library('email');

        $config = Array(
            'protocol' => 'mail',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'pravi0025@gmail.com',
            'smtp_pass' => 'pravi123456',
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'newline' => "\r\n",
            'wordwrap' => TRUE,
            'validation' => TRUE
        );

        $this->email->initialize($config);

        $this->email->from($email, $name);
        $this->email->to('connect@bizstartdubai.com');

        $this->email->subject('BizStart Conatct Form');
        $this->email->message($mail_body);

        $this->email->send(false);

        $this->session->set_flashdata('success', 'Thank you. Your Details has been sent. We will contact you soon');
        redirect(site_url('contact'));
    }

    public function get_appoinment(){
        $name = $this->input->post('name');  
        $mobile = $this->input->post('mobile');
        $email = $this->input->post('email');
        $message = $this->input->post('message');
        $data = array('name' => $name, 
                    'mobile' => $mobile, 
                    'email' =>$email,
                    'message' => $message
                );
        $mail_body = $this->load->view('mail/appoinment_form', $data, TRUE);

        $this->load->library('email');

        $config = Array(
            'protocol' => 'mail',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'pravi0025@gmail.com',
            'smtp_pass' => 'pravi123456',
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'newline' => "\r\n",
            'wordwrap' => TRUE,
            'validation' => TRUE
        );

        $this->email->initialize($config);

        $this->email->from($email, $name);
        $this->email->to('connect@bizstartdubai.com');

        $this->email->subject('BizStart Appoinment Form');
        $this->email->message($mail_body);

        $this->email->send(false);

        $this->session->set_flashdata('success', 'Thank you. Your Details has been sent. We will contact you soon');
        redirect(site_url());
    }

    public function signup(){
        $name = $this->input->post('name');  
        $mobile = $this->input->post('mobile');
        $email = $this->input->post('email');
        $message = $this->input->post('message');
        $refernce = rand(1,99999);
        $data = array('name' => $name, 
                    'mobile' => $mobile, 
                    'email' => $email,
                    'message' => $message,
                    'refernce' => $refernce
                );
        $mail_body = $this->load->view('mail/signup_form', $data, TRUE);

        $this->load->library('email');

        $config = Array(
            'protocol' => 'mail',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'pravi0025@gmail.com',
            'smtp_pass' => 'pravi123456',
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'newline' => "\r\n",
            'wordwrap' => TRUE,
            'validation' => TRUE
        );

        $this->email->initialize($config);

        $this->email->from($email, $name);
        $this->email->to($email);

        $this->email->subject('BizStart Signup Details');
        $this->email->message($mail_body);

        $this->email->send(false);

        $mail_body1 = $this->load->view('mail/signupadmin_form', $data, TRUE);
        $this->email->from($email, $name);
        $this->email->to('connect@bizstartdubai.com');
        $this->email->subject('BizStart Signup Details');
        $this->email->message($mail_body1);

        $this->email->send(false);
        $this->session->set_flashdata('success', 'Thank you. Your Details has been sent. We will contact you soon');
        redirect(site_url());
    }

    public function commercial_license()
    {
        $data['data'] = $this->data;
        $data['view_page'] = 'home/commercial';
        $this->load->view('home/template', $data);
    }

    public function business_centers()
    {
        $data['data'] = $this->data;
        $data['view_page'] = 'home/business_centers';
        $this->load->view('home/template', $data);
    }

    public function refer()
    {
        $data['data'] = $this->data;
        $data['view_page'] = 'home/refer';
        $this->load->view('home/template', $data);
    }

    public function added_service()
    {
        $data['data'] = $this->data;
        $data['view_page'] = 'home/added_service';
        $this->load->view('home/template', $data);
    }

    public function dubai_business()
    {
        $data['data'] = $this->data;
        $data['view_page'] = 'home/dubai_business';
        $this->load->view('home/template', $data);
    }

    public function sharjah_business()
    {
        $data['data'] = $this->data;
        $data['view_page'] = 'home/sharjah_business';
        $this->load->view('home/template', $data);
    }

    public function fujeirah_business()
    {
        $data['data'] = $this->data;
        $data['view_page'] = 'home/fujeirah_business';
        $this->load->view('home/template', $data);
    }

    public function ajman_business()
    {
        $data['data'] = $this->data;
        $data['view_page'] = 'home/ajman_business';
        $this->load->view('home/template', $data);
    }

    public function ras_al_khaima_business()
    {
        $data['data'] = $this->data;
        $data['view_page'] = 'home/ras_al_khaima_business';
        $this->load->view('home/template', $data);
    }

    public function abudhabi_business()
    {
        $data['data'] = $this->data;
        $data['view_page'] = 'home/abudhabi_business';
        $this->load->view('home/template', $data);
    }

    public function ded_business()
    {
        $data['data'] = $this->data;
        $data['view_page'] = 'home/ded_business';
        $this->load->view('home/template', $data);
    }

    public function dwc_business()
    {
        $data['data'] = $this->data;
        $data['view_page'] = 'home/dwc_business';
        $this->load->view('home/template', $data);
    }

    public function dmcc_business()
    {
        $data['data'] = $this->data;
        $data['view_page'] = 'home/dmcc_business';
        $this->load->view('home/template', $data);
    }

    public function saif_business()
    {
        $data['data'] = $this->data;
        $data['view_page'] = 'home/saif_business';
        $this->load->view('home/template', $data);
    }

    public function jabel_business()
    {
        $data['data'] = $this->data;
        $data['view_page'] = 'home/jabel_business';
        $this->load->view('home/template', $data);
    }

    public function silicon_business()
    {
        $data['data'] = $this->data;
        $data['view_page'] = 'home/silicon_business';
        $this->load->view('home/template', $data);
    }

    public function flexi_desk()
    {
        $data['data'] = $this->data;
        $data['view_page'] = 'home/flexi_desk';
        $this->load->view('home/template', $data);
    }

    public function rental_office()
    {
        $data['data'] = $this->data;
        $data['view_page'] = 'home/rental_office';
        $this->load->view('home/template', $data);
    }

    public function meeting_room()
    {
        $data['data'] = $this->data;
        $data['view_page'] = 'home/meeting_room';
        $this->load->view('home/template', $data);
    }

    public function co_working()
    {
        $data['data'] = $this->data;
        $data['view_page'] = 'home/co_working';
        $this->load->view('home/template', $data);
    }

}
