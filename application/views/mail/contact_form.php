<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style type="text/css">
        table{
            width: 90%;
            border-collapse: collapse;
            margin: 0 auto;
        }
        table td,
        table th{
            padding: 10px;
            background-color: #fcfcfc;
            border: 1px solid #d8d8d8;
            text-align: left;
            word-break: break-all;
            word-wrap: break-word;
        }

        div.wrapper{
            background:#f5f5f5;
            border-radius: 5px;
            padding: 1%;
        }

        h2{
            text-transform: uppercase;
            text-decoration: underline;
        }

    </style>
</head>
<body>

    <div class="wrapper" style="background-color: #f2f2f2;padding: 15px 15px">
        <h2 style="text-align: center"> User Details </h2>
        <table style="border: 1px solid #ddd;width: 100%;border-collapse: collapse;">
            <tr style="border-bottom: 1px solid #ddd;">
                <th style="width:40%;padding: 10px;border-right: 1px solid #ddd">Name</th>
                <td style="width:60%;padding: 10px"><?= $name ?></td>
            </tr>
            <tr style="border-bottom: 1px solid #ddd;">
                <th style="width:40%;padding: 10px;border-right: 1px solid #ddd">Email</th>
                <td style="width:60%;padding: 10px"><?= $email ?></td>
            </tr>
            <tr style="border-bottom: 1px solid #ddd;">
                <th style="width:40%;padding: 10px;border-right: 1px solid #ddd">Phone Number</th>
                <td style="width:60%;padding: 10px"><?= $phone ?></td>
            </tr>
            <tr style="border-bottom: 1px solid #ddd;">
                <th style="width:40%;padding: 10px;border-right: 1px solid #ddd">Message</th>
                <td style="width:60%;padding: 10px"><?= $message ?></td>
            </tr>
        </table>
        <table style="width: 100%">
            <tr>
                <td style="text-align: center;">
                    copyright © <a href="<?= site_url() ?>">BizStart Dubai</a>
                </td>
            </tr>
        </table>
    </div>
    
</body>
</html>