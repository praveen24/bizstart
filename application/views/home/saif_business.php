<div class="container-fluid">
	<div class="row breadcrumb-section">
		<div class="container">
			<div class="row">
				<div class="col-sm-10">
					<ul class="breadcrumb">
					    <li><a href="<?=site_url()?>">Home</a></li>
					    <li><a href="#">Business Setup in UAE</a></li>
					    <li class="active">Business Setup in Saif Zone</li>
					</ul>
				</div>
				<div class="col-sm-2">
					<div class="pull-right sm-pull-none mb-sm-15">
                        <button class="hvr-sweep-to-right btn btn-dark btn-theme-colored2 font-14" data-target="#signupModal" data-toggle="modal">SIGN UP</button>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row pt-50 pb-40">
		<div class="col-sm-12">
			<h2>Business set up in Saif Zone</h2>
			<p class="text-justify">Sharjah Airport International Freezone (SAIF) is one of the premier business destinations of the region, Sharjah Airport International Free Zone has been the location of choice for more than 5,300 companies from 158 countries. Established in 1995, Sharjah Airport International Free Zone is located in close proximity to the Sharjah International Airport and the seaports, thus providing marketing access to over 2 billion consumers from the GCC, Mediterranean, Africa and India.</p>
			<p class="text-justify">SAIF Zone allows three types of business company types or business entities within its premises:</p>
			<h4><strong>Free Zone Establishment (FZE)</strong></h4>
			<p class="text-justify">Formed with one shareholder, either an individual or a company. The minimum capital requirement depends on the activity.</p>
			<h4><strong>Free Zone Company (FZCO)</strong></h4>
			<p class="text-justify">Formed by a minimum of two and maximum of five shareholders, either individuals or companies or a combination of both. The minimum capital requirement depends on the activity.</p>
			<h4>Branch Office of an Existing company</h4>
			<p class="text-justify">Formed as a branch of an existing foreign company in Dubai Airport Free Zone with no share capital required.</p>
			<span><strong>Benefits of SAIF Zone</strong></span>
			<ul class="ul_listing" style="margin-top: 10px;">
				<li>100% foreign ownership</li>
				<li>100% profit and capital repatriation</li>
				<li>100% tax exemption</li>
				<li>Easy license issuance (24 hours)</li>
				<li>Visa and sponsorship for staff (no limit for expat)</li>
			</ul>
		</div>
	</div>
</div>