<div id="wrapper">
   
    <!-- Header -->
    <header id="header" class="header">
        <div class="header-top bg-theme-colored sm-text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="widget text-white">
                            <i class="fa fa-clock-o text-theme-colored2"></i> Opening Hours:  Sat - Thu : 8.00 AM to 8.00 PM, Friday Closed
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="widget pull-right flip sm-pull-none">
                            <ul class="list-inline  text-right flip sm-text-center">
                                <li class="m-0 pl-10"> <a href="#" class="text-white"> Blogs</a></li>
                                <li class="m-0 pl-0 pr-10"> 
                                    <a href="<?= site_url('contact') ?>" class="text-white">Contact Us</a> 
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-middle border-bottom xs-text-center p-0">
            <div class="container pt-15 pb-15">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-4">
                        <a class="menuzord-brand pull-left flip sm-pull-center mt-10" href="<?= site_url() ?>"><img src="<?= base_url('assets/images/logo-wide1.png') ?>" alt=""></a>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-8">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="widget no-border sm-text-center mt-10 mb-10 m-0">
                                    <i class="pe-7s-call text-theme-colored2 bg-silver-light border-1px font-28 font-weight-800 mr-15 mr-sm-0 sm-display-block pull-left flip sm-pull-none p-10"></i>
                                    <a href="#" class="text-theme-colored font-weight-600 text-uppercase">Call Us Now</a>
                                    <h5 class="font-12 m-0"> +971 58 908 5535<br>+971 4  239 4771</h5>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="widget no-border sm-text-center mt-10 mb-10 m-0">
                                    <i class="pe-7s-map-marker text-theme-colored2 bg-silver-light border-1px font-28 font-weight-800 mr-15 mr-sm-0 sm-display-block pull-left flip sm-pull-none p-10"></i>
                                    <a href="#" class="office text-theme-colored font-weight-600 text-uppercase">Office Adress</a>
                                    <h5 class="font-12 m-0 off_addr">P.O.Box 18938, 12A (06), Damac Executive Heights, Dubai</h5>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="widget no-border sm-text-center mt-10 mb-10 m-0">
                                    <i class="pe-7s-mail-open text-theme-colored2 bg-silver-light border-1px font-28 font-weight-800 mr-15 mr-sm-0 sm-display-block pull-left flip sm-pull-none p-10"></i>
                                    <a href="#" class="text-theme-colored font-weight-600 text-uppercase">Mail Us Today</a>
                                    <h5 class="font-12 m-0">connect@bizstartdubai.com</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-nav">
            <div class="header-nav-wrapper navbar-scrolltofixed bg-white">
                <div class="container">
                    <nav id="menuzord" class="menuzord red">
                        <ul class="menuzord-menu onepage-nav">
                            <li class="<?= $this->uri->segment(1) == '' ? 'active' : '' ?>"><a href="<?= site_url() ?>">Home</a></li>
                            <li class="<?= $this->uri->segment(1) == 'about-us' ? 'active' : '' ?>"><a href="<?= site_url('about-us') ?>">About Us</a></li>
                            <?php
                                $wwa = in_array($this->uri->segment(1), [
                                    'uae-mainland-business-setup',
                                    'business-setup-in-uae-freezone',
                                    'offshore-business-setup-in-dubai'
                                ]); 
                            ?>
                            <li class="<?= $wwa ? 'active' : '' ?>"><a href="#">Business Licenses</a>
                                <span class="fa fa-angle-down"></span>
                                <ul class="subMenu">
                                    <li><a href="<?= site_url('uae-mainland-business-setup') ?>">UAE Mainland</a></li>
                                    <li><a href="<?= site_url('business-setup-in-uae-freezone') ?>">UAE Freezone</a></li>
                                    <li><a href="<?= site_url('offshore-business-setup-in-dubai') ?>">Offshore Business</a></li>
                                </ul>
                            </li>
                            <li class="<?= $this->uri->segment(1) == 'added-service' ? 'active' : '' ?>"><a href="<?= site_url('added-service') ?>">Added Services</a>
                                <!-- <span class="fa fa-angle-down"></span>
                                <ul class="subMenu">
                                    <li><a href="#">Pro & Visa Services</a></li>
                                    <li><a href="#">Paperwork & Translation</a></li>
                                    <li><a href="#">Trademark Registration</a></li>
                                    <li><a href="#">Branding</a></li>
                                    <li><a href="#">Website Design & Development</a></li>
                                    <li><a href="#">Digital Marketing in UAE</a></li>
                                    <li><a href="#">VAT Book Keeping & Accounting</a></li>
                                    <li><a href="#">Business Consulting in UAE</a></li>
                                    <li><a href="#">Market Analysis in UAE</a></li>
                                    <li><a href="#">Corporate Lawyers and Law Practices</a></li>
                                </ul> -->
                            </li>
                            <?php
                                $b = in_array($this->uri->segment(1), [
                                    'business-setup-in-dubai',
                                    'business-setup-in-sharjah',
                                    'business-setup-in-fujairah',
                                    'business-setup-in-ajman',
                                    'business-setup-in-ras-al-khaimah',
                                    'business-setup-in-abudhabi',
                                    'business-setup-in-ded',
                                    'business-setup-in-dwc',
                                    'business-setup-in-dmcc',
                                    'business-setup-in-saif-zone',
                                    'business-setup-in-jabel-ali',
                                    'business-setup-in-silicon-oasis'
                                ]); 
                            ?>
                            <li class="<?= $b ? 'active' : '' ?>"><a href="#">Business Setup in UAE </a>
                                <span class="fa fa-angle-down"></span>
                                <ul class="subMenu">
                                    <li><a href="<?= site_url('business-setup-in-dubai') ?>">Business Setup in Dubai</a></li>
                                    <li><a href="<?= site_url('business-setup-in-ded') ?>">DED Business Setup</a></li>
                                    <li><a href="<?= site_url('business-setup-in-dwc') ?>">DWC Business Setup</a></li>
                                    <li><a href="<?= site_url('business-setup-in-dmcc') ?>">Business Setup in DMCC</a></li>
                                    <li><a href="<?= site_url('business-setup-in-abudhabi') ?>">Business Setup in Abudhabi</a></li>
                                    <li><a href="<?= site_url('business-setup-in-fujairah') ?>">Business Setup in Fujairah</a></li>
                                    <li><a href="<?= site_url('business-setup-in-ajman') ?>">Business Setup in Ajman</a></li>
                                    <li><a href="<?= site_url('business-setup-in-silicon-oasis') ?>">Business Setup in Silicon Oasis</a></li>
                                    <li><a href="<?= site_url('business-setup-in-sharjah') ?>">Business Setup in Sharjah</a></li>
                                    <li><a href="<?= site_url('business-setup-in-ras-al-khaimah') ?>">Business Setup in Ras Al Khaimah</a></li>
                                    <li><a href="<?= site_url('business-setup-in-saif-zone') ?>">Business Setup in Saif</a></li>
                                    <li><a href="<?= site_url('business-setup-in-jabel-ali') ?>">Business Setup in Jabel Ali</a></li>
                                </ul>
                            </li>
                            <?php
                                $wwc = in_array($this->uri->segment(1), [
                                    'flexi-desk-in-uae',
                                    'office-for-rent-in-uae',
                                    'meeting-rooms-in-uae',
                                    'business-centers-in-uae',
                                    'co-working-space-in-uae'
                                    
                                ]); 
                            ?>
                            <li class="<?= $wwc ? 'active' : '' ?>"><a href="#">Rental Offices</a>
                                <span class="fa fa-angle-down"></span>
                                <ul class="subMenu">
                                    <li><a href="<?= site_url('flexi-desk-in-uae') ?>">Flexi-Desk</a></li>
                                    <li><a href="<?= site_url('office-for-rent-in-uae') ?>">Rental Offices</a></li>
                                    <li><a href="<?= site_url('meeting-rooms-in-uae') ?>">Meeting Rooms</a></li>
                                    <li><a href="<?= site_url('business-centers-in-uae') ?>">Business Centres</a></li>
                                    <li><a href="<?= site_url('co-working-space-in-uae') ?>">Co-working Spaces</a></li>
                                </ul>
                            </li>
                            <li class="<?= $this->uri->segment(1) == 'refer-and-earn' ? 'active' : '' ?>"><a href="<?= site_url('refer-and-earn') ?>">Refer & Earn</a></li>
                        </ul>
                        <div class="pull-right sm-pull-none mb-sm-15">
                            <button class="btn btn-app mt-15" data-target="#appointmentModal" data-toggle="modal">GET APPOINTMENT</button>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>