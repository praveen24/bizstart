<div class="container-fluid">
	<div class="row breadcrumb-section">
		<div class="container">
			<div class="row">
				<div class="col-sm-10">
					<ul class="breadcrumb">
					    <li><a href="<?=site_url()?>">Home</a></li>
					    <li><a href="#">Start a Company</a></li>
					    <li><a href="#">Where to start a company in the UAE?</a></li>
					    <li class="active">UAE Offshore</li>
					</ul>
				</div>
				<div class="col-sm-2">
					<div class="pull-right sm-pull-none mb-sm-15">
                        <button class="hvr-sweep-to-right btn btn-dark btn-theme-colored2 font-14" data-target="#signupModal" data-toggle="modal">SIGN UP</button>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row pt-50 pb-40">
		<div class="col-sm-12">
			<h2>UAE Offshore Business Setup</h2>
			<!-- <p>An offshore company is flexible enough to have a mixture of features. Though offshore registrars are structured under free zone and it enjoys benefits that a free zone company does in terms of ownership, it cannot be used as a direct substitute for a free zone company.</p> -->
			<p class="text-justify">By availing our Offshore Company setup services in UAE, you will be able to keep your focus on how you want to do business in the emirate, while our professional experts take care of everything else for you. You certainly can explore a wider range of opportunities while doing so, and if you need any aid at any stage, we will be there to help you out in the most efficient way.</p>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3613.180226136265!2d55.17074981448218!3d25.095759841967947!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f6b7af1ca6c7d%3A0xb992366792daa99c!2sDAMAC+Executive+Heights!5e0!3m2!1sen!2sin!4v1540797742074" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
</div>