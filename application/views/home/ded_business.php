<div class="container-fluid">
	<div class="row breadcrumb-section">
		<div class="container">
			<div class="row">
				<div class="col-sm-10">
					<ul class="breadcrumb">
					    <li><a href="<?=site_url()?>">Home</a></li>
					    <li><a href="#">Business Setup in UAE</a></li>
					    <li class="active">Business Setup in DED</li>
					</ul>
				</div>
				<div class="col-sm-2">
					<div class="pull-right sm-pull-none mb-sm-15">
                        <button class="hvr-sweep-to-right btn btn-dark btn-theme-colored2 font-14" data-target="#signupModal" data-toggle="modal">SIGN UP</button>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row pt-50 pb-40">
		<div class="col-sm-12">
			<h2>Business set up in DED</h2>
			<p class="text-justify">UAE local market is called Mainland and consists of all areas which are accessible to all local traders, distributors and suppliers. All licenses in mainland area are issued by the respective Emirates's Department of Economic Development (DED). DED issues the trade license under following categories:</p>
			<ul class="ul_listing">
				<li>Commercial</li>
				<li>Professional</li>
				<li>Industrial</li>
			</ul>
		</div>
	</div>
</div>