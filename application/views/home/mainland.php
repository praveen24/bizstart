<div class="container-fluid">
	<div class="row breadcrumb-section">
		<div class="container">
			<div class="row">
				<div class="col-sm-10">
					<ul class="breadcrumb">
					    <li><a href="<?=site_url()?>">Home</a></li>
					    <li><a href="#">Start a Company</a></li>
					    <li><a href="#">Where to start a company in the UAE?</a></li>
					    <li class="active">UAE Mainland</li>
					</ul>
				</div>
				<div class="col-sm-2">
					<div class="pull-right sm-pull-none mb-sm-15">
                        <button class="hvr-sweep-to-right btn btn-dark btn-theme-colored2 font-14" data-target="#signupModal" data-toggle="modal">SIGN UP</button>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row pt-50 pb-40">
		<div class="col-sm-12">
			<h2>UAE Mainland Business Setup</h2>
			<p class="text-justify">Establishing a business in UAE is an excellent way of expanding business horizons, making a global standing and legitimately booking international profits with global presence. Dubai Mainland</p>
			<p class="text-justify">Bizstart Dubai offers a complete one-stop solution from the start to the finish, offering customized, reliable and hassle-free solutions for all your business needs.</p>
			<!-- <h3>Process</h3>
			<p>In mainland business setup process, if you want to setup your company in the Dubai mainland, you will need to appoint a UAE national as a local sponsor for your business. However, the local sponsor or sleeping partner will make an agreement that he will not have any involvement in the company operations or profit sharing of the company setup in Dubai mainland.</p>
			<p>For a professional License, the investor can have 100% ownership, but a local service agent is still required. Thus for a professional company, the local service agent has no active part in the company business management, operations or profit sharing. Instead, they will act as figurehead representative, while drafting legal agreements and establishing other business documentation required in Dubai.</p> -->
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3613.180226136265!2d55.17074981448218!3d25.095759841967947!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f6b7af1ca6c7d%3A0xb992366792daa99c!2sDAMAC+Executive+Heights!5e0!3m2!1sen!2sin!4v1540797742074" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
</div>