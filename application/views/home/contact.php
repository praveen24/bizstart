<div class="container-fluid">
	<div class="row breadcrumb-section">
		<div class="container">
			<div class="row">
				<div class="col-sm-10">
					<ul class="breadcrumb">
					    <li><a href="<?=site_url()?>">Home</a></li>
					    <li class="active">Contact Us</li>
					</ul>
				</div>
				<div class="col-sm-2">
					<div class="pull-right sm-pull-none mb-sm-15">
                        <button class="hvr-sweep-to-right btn btn-dark btn-theme-colored2 font-14" data-target="#signupModal" data-toggle="modal">SIGN UP</button>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<section class="contact-info">
	<div class="container pt-50 pb-50">
		<div class="row">
			<div class="contact-info-list">
				<div class="col-md-4">
                    <i class="pe-7s-map-marker text-theme-colored2 bg-silver-light border-1px font-28 font-weight-800 mr-15 mr-sm-0 sm-display-block pull-left flip sm-pull-none p-10"></i> P.O.Box 18938, 12A (06), Damac Executive Heights Dubai, UAE
                </div>
                <div class="col-md-4">
                    <i class="pe-7s-call text-theme-colored2 bg-silver-light border-1px font-28 font-weight-800 mr-15 mr-sm-0 sm-display-block pull-left flip sm-pull-none p-10"></i> +971 58 908 5535
                    <div class="clearfix"></div>+971 4  239 4771
                </div>
                <div class="col-md-4">
                    <i class="pe-7s-mail-open text-theme-colored2 bg-silver-light border-1px font-28 font-weight-800 mr-15 mr-sm-0 sm-display-block pull-left flip sm-pull-none p-10"></i> connect@bizstartdubai.com<br>
					&nbsp;
                </div> 
			</div>
		</div>
	</div>
</section>
<section class="contact-box">
	<div class="container pt-50 pb-50">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center">
				<h2>Contact Now!</h2>
				<h5>BizStartDubai works and thinks the way you do</h5>
				<h5>We take your ideas and business seriously. Our representatives are here to assist you.</h5>
				<?php if ($this->session->flashdata('success')) : ?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php echo $this->session->flashdata('success'); ?>
					</div>
				<?php elseif ($this->session->flashdata('error')) : ?>
					<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php echo $this->session->flashdata('error'); ?>
					</div>
				<?php elseif ($this->session->flashdata('info')) : ?>
					<div class="alert alert-info alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php echo $this->session->flashdata('info'); ?>
					</div>
				<?php endif; ?>
				<form action="<?= site_url('contact_info') ?>" method="post">
					<div class="row">
						<div class="col-md-12 form-group">
							<input type="text" name="name" placeholder="Name" class="form-control">
						</div>
						<div class="col-md-6 form-group">
							<input type="email" name="email" placeholder="Email" class="form-control">
						</div>
						<div class="col-md-6 form-group">
							<input type="text" name="phone" placeholder="Phone" class="form-control">
						</div>
						<div class="col-md-12 form-group">
							<textarea rows="4" class="form-control" placeholder="Message" name="message"></textarea>
						</div>
						<div class="form-group col-md-12">
							<button class="hvr-sweep-to-right btn btn-dark btn-theme-colored2 btn-lg font-18">SUBMIT NOW</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<div class="container-fluid">
	<div class="row">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3613.180226136265!2d55.17074981448218!3d25.095759841967947!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f6b7af1ca6c7d%3A0xb992366792daa99c!2sDAMAC+Executive+Heights!5e0!3m2!1sen!2sin!4v1540797742074" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
</div>