<div class="container-fluid">
	<div class="row breadcrumb-section">
		<div class="container">
			<div class="row">
				<div class="col-sm-10">
					<ul class="breadcrumb">
					    <li><a href="<?=site_url()?>">Home</a></li>
					    <li class="active">Added Service</li>
					</ul>
				</div>
				<div class="col-sm-2">
					<div class="pull-right sm-pull-none mb-sm-15">
                        <button class="hvr-sweep-to-right btn btn-dark btn-theme-colored2 font-14" data-target="#signupModal" data-toggle="modal">SIGN UP</button>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row pt-50 pb-40">
		<div class="col-sm-12">
			<h2>Added Services</h2>
			<p class="text-justify">From trademark registration to market analysis, we provide a range of end-to-end support for your business. We aim to be a one stop shop, providing you with convenience and ease of access to a range of professional services so that you can focus on running your business leaving rest of the things on us.</p>
			<p class="text-justify">We provide each client with personalized, quality service that is beyond comparison. Whatever be your market, we can offer you everything you need for your businesses. Here's the list of services that we specialize in:</p>
			<ul class="ul_listing">
				<li><a href="#" data-target="#signupModal" data-toggle="modal">PRO & Visa Services</a></li>
				<li><a href="#" data-target="#signupModal" data-toggle="modal">Paperwork & Translations</a></li>
				<li><a href="#" data-target="#signupModal" data-toggle="modal">Trademark Registration</a></li>
				<li><a href="#" data-target="#signupModal" data-toggle="modal">Branding</a></li>
				<li><a href="#" data-target="#signupModal" data-toggle="modal">Website Design & Development</a></li>
				<li><a href="#" data-target="#signupModal" data-toggle="modal">Digital Marketing in UAE</a></li>
				<li><a href="#" data-target="#signupModal" data-toggle="modal">VAT Book Keeping & Accounting</a></li>
				<li><a href="#" data-target="#signupModal" data-toggle="modal">Business Consulting in UAE</a></li>
				<li><a href="#" data-target="#signupModal" data-toggle="modal">Market Analysis in UAE</a></li>
				<li><a href="#" data-target="#signupModal" data-toggle="modal">Corporate Lawyers & Law Practise</a></li>
			</ul>
		</div>
	</div>
</div>