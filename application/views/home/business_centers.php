<div class="container-fluid">
	<div class="row breadcrumb-section">
		<div class="container">
			<div class="row">
				<div class="col-sm-10">
					<ul class="breadcrumb">
					    <li><a href="<?=site_url()?>">Home</a></li>
					    <li class="active">Business Centers</li>
					</ul>
				</div>
				<div class="col-sm-2">
					<div class="pull-right sm-pull-none mb-sm-15">
                        <button class="hvr-sweep-to-right btn btn-dark btn-theme-colored2 font-14" data-target="#signupModal" data-toggle="modal">SIGN UP</button>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row pt-50 pb-40">
		<div class="col-sm-6">
			<img src="<?= base_url('assets/images/center.jpg') ?>" class="img-responsive">
		</div>
		<div class="col-sm-6">
			<h3 class="mt-0">Business centers for rent in UAE</h3>
			<p class="text-justify">Our Business Starter Package includes a DED Professional license, 3-year residence visa, and registration within our co-working facility with all services included.</p>
			<strong>Services included:</strong>
			<ul class="ul_listing">
				<li>DED Professional License</li>
				<li>3-year Investor Visa Package</li>
				<li>Local Service Agent</li>
				<li>Business Centre registration</li>
				<li>Co-working dedicated desk</li>
				<li>All inclusive package</li>
				<li>Meeting room</li>
				<li>Dedicated switchboard number </li>
			</ul>
			<p class="text-justify">Contact us to find out more about the cost savings and numerous other benefits.</p>
			<strong>* Actual license costs may vary</strong>
		</div>
	</div>
</div>