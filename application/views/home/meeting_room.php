<div class="container-fluid">
	<div class="row breadcrumb-section">
		<div class="container">
			<div class="row">
				<div class="col-sm-10">
					<ul class="breadcrumb">
					    <li><a href="<?=site_url()?>">Home</a></li>
					    <li><a href="#">Rental Offices</a></li>
					    <li class="active">Meeting Room</li>
					</ul>
				</div>
				<div class="col-sm-2">
					<div class="pull-right sm-pull-none mb-sm-15">
                        <button class="hvr-sweep-to-right btn btn-dark btn-theme-colored2 font-14" data-target="#signupModal" data-toggle="modal">SIGN UP</button>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row pt-50 pb-40">
		<div class="col-sm-6">
			<img src="<?= base_url('assets/images/meeting.jpg') ?>" class="img-responsive">
		</div>
		<div class="col-sm-6">
			<h2 class="mt-0">Meeting Rooms in UAE</h2>
			<p class="text-justify">We offer meeting rooms of various sizes and configurations, depending on what best suits your purposes. Whether you need to organise an interview, huddle group, off-site, board meeting, training, or negotiations, we have the perfect space for you.</p>
			<ul class="ul_listing">
				<li>Premium location</li>
				<li>Multiple meeting room sizes</li>
				<li>Projection equipment</li>
				<li>Professional receptionists</li>
				<li>Meet & greet service for guests</li>
				<li>Complimentary beverages</li>
				<li>Catering services </li>
			</ul>
			<p>Contact us to find out more about the cost savings and numerous other benefits.</p>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3613.180226136265!2d55.17074981448218!3d25.095759841967947!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f6b7af1ca6c7d%3A0xb992366792daa99c!2sDAMAC+Executive+Heights!5e0!3m2!1sen!2sin!4v1540797742074" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
</div>