	  <!-- Start main-content -->
    <div class="main-content"> 
        <!-- Section: home -->
        <section id="home" class="divider">
            <div class="container-fluid p-0">
                <!-- START REVOLUTION SLIDER 5.0.7 -->
                <div id="rev_slider_home_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="news-gallery34" style="margin:0px auto;background-color:#ffffff;padding:0px;margin-top:0px;margin-bottom:0px;">
                    <!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
                    <div id="rev_slider_home" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
                        <ul>
                            <!-- SLIDE 1 -->
                            <li data-index="rs-1" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="<?= base_url('assets/images/bg/bg4.jpg') ?>" data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="Make an Impact">
                                <!-- MAIN IMAGE -->
                                <img src="<?= base_url('assets/images/bg/bg4.jpg') ?>" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                                <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme rs-parallaxlevel-0" 
                                id="slide-1-layer-1" 
                                data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                                data-width="full"
                                data-height="full"
                                data-whitespace="normal"
                                data-transform_idle="o:1;"
                                data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;" 
                                data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                                data-start="1000" 
                                data-basealign="slide" 
                                data-responsive_offset="on" 
                                style="z-index: 5;background-color:rgba(0, 0, 0, 0.35);border-color:rgba(0, 0, 0, 1.00);"> 
                                </div>
                                <div class="tp-caption tp-resizeme text-white rs-parallaxlevel-0" 
                                  id="slide-1-layer-2" 
                                  data-x="['left','left','left','left']" data-hoffset="['50','50','50','30']" 
                                  data-y="['top','top','top','top']" data-voffset="['120','100','70','90']" 
                                  data-fontsize="['56','46','40','36']"
                                  data-lineheight="['70','60','50','45']"
                                  data-fontweight="['800','700','700','700']"
                                  data-width="['700','650','600','420']"
                                  data-height="none"
                                  data-whitespace="normal"
                                  data-transform_idle="o:1;"
                                  data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                                  data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                                  data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                                  data-start="1000" 
                                  data-splitin="none" 
                                  data-splitout="none" 
                                  data-responsive_offset="on" 
                                  style="z-index: 6; min-width: 600px; max-width: 600px; white-space: normal;">We nurture <span class="text-theme-colored2">Steve Jobs</span><br> and <span class="text-theme-colored2">Bill Gates</span>.
                                </div>
                                <!-- <div class="tp-caption tp-resizeme text-white rs-parallaxlevel-0" 
                                  id="slide-1-layer-3" 
                                  data-x="['left','left','left','left']" data-hoffset="['50','50','50','30']" 
                                  data-y="['top','top','top','top']" data-voffset="['280','220','180','180']" 
                                  data-fontsize="['18','18','16','13']"
                                  data-lineheight="['30','30','28','25']"
                                  data-fontweight="['500','500','500','500']"
                                  data-width="['800','650','600','420']"
                                  data-height="none"
                                  data-whitespace="nowrap"
                                  data-transform_idle="o:1;"
                                  data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                                  data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                                  data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                                  data-start="1000" 
                                  data-splitin="none" 
                                  data-splitout="none" 
                                  data-responsive_offset="on" 
                                  style="z-index: 7; white-space: nowrap;">We provides always our best services for our clients and  always<br> try to achieve our client's trust and satisfaction.
                                </div> -->
                                <!-- LAYER NR. 4 -->
                                <!-- <div class="tp-caption rs-parallaxlevel-0" 
                                  id="slide-1-layer-4" 
                                  data-x="['left','left','left','left']" data-hoffset="['53','53','53','30']" 
                                  data-y="['top','top','top','top']" data-voffset="['360','290','260','260']" 
                                  data-width="none"
                                  data-height="none"
                                  data-whitespace="nowrap"
                                  data-transform_idle="o:1;"
                                  data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
                                  data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                                  data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                                  data-mask_in="x:0px;y:0px;" 
                                  data-mask_out="x:0;y:0;" 
                                  data-start="1000" 
                                  data-splitin="none" 
                                  data-splitout="none" 
                                  data-responsive_offset="on" 
                                  data-responsive="off"
                                  style="z-index: 8; white-space: nowrap;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;"><a href="#" class="btn btn-default btn-theme-colored2 btn-xl">Read More</a> <a href="#" class="btn btn-dark btn-theme-colored2 btn-xl">Register Now</a>
                                </div> -->
                            </li>
                            <li data-index="rs-2" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="<?= base_url('assets/images/bg/bg5.jpg') ?>" data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="Make an Impact">
                                <img src="<?= base_url('assets/images/bg/bg5.jpg') ?>" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                                <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme rs-parallaxlevel-0" 
                                  id="slide-2-layer-1" 
                                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                  data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                                  data-width="full"
                                  data-height="full"
                                  data-whitespace="normal"
                                  data-transform_idle="o:1;"
                                  data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;" 
                                  data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                                  data-start="1000" 
                                  data-basealign="slide" 
                                  data-responsive_offset="on" 
                                  style="z-index: 5;background-color:rgba(0, 0, 0, 0.25);border-color:rgba(0, 0, 0, 1.00);"> 
                                </div>
                                <!-- LAYER NR. 2 -->
                                <div class="tp-caption tp-resizeme text-white rs-parallaxlevel-0" 
                                  id="slide-2-layer-2" 
                                  data-x="['center','center','center','center']" data-hoffset="['310','200','100','0']" 
                                  data-y="['top','top','top','top']" data-voffset="['120','100','70','90']" 
                                  data-fontsize="['56','46','40','36']"
                                  data-lineheight="['70','60','50','45']"
                                  data-fontweight="['800','700','700','700']"
                                  data-width="['700','650','500','420']"
                                  data-height="none"
                                  data-whitespace="normal"
                                  data-transform_idle="o:1;"
                                  data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                                  data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                                  data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                                  data-start="1000" 
                                  data-splitin="none" 
                                  data-splitout="none" 
                                  data-responsive_offset="on" 
                                  style="z-index: 6; min-width: 600px; max-width: 600px; white-space: normal;">Ideas are born on <span class="text-theme-colored2">your head,</span> we nurture it on the fertile land of UAE.
                                </div>
                                <!-- LAYER NR. 3 -->
                                <!-- <div class="tp-caption tp-resizeme text-white rs-parallaxlevel-0" 
                                  id="slide-2-layer-3" 
                                  data-x="['center','center','center','center']" data-hoffset="['310','200','100','0']" 
                                  data-y="['top','top','top','top']" data-voffset="['280','220','180','180']" 
                                  data-fontsize="['18','18','16','13']"
                                  data-lineheight="['30','30','28','25']"
                                  data-fontweight="['500','500','500','500']"
                                  data-width="['700','650','500','420']"
                                  data-height="none"
                                  data-whitespace="nowrap"
                                  data-transform_idle="o:1;"
                                  data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                                  data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                                  data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                                  data-start="1000" 
                                  data-splitin="none" 
                                  data-splitout="none" 
                                  data-responsive_offset="on" 
                                  style="z-index: 7; white-space: nowrap;">We provides always our best services for our clients and  always<br> try to achieve our client's trust and satisfaction.
                                </div> -->
                                <!-- LAYER NR. 4 -->
                                <!-- <div class="tp-caption tp-resizeme text-white rs-parallaxlevel-0" 
                                  id="slide-2-layer-4" 
                                  data-x="['center','center','center','center']" data-hoffset="['310','200','100','0']" 
                                  data-y="['top','top','top','top']" data-voffset="['360','290','260','260']" 
                                  data-fontsize="['18','18','16','16']"
                                  data-lineheight="['30','30','30','30']"
                                  data-fontweight="['600','600','600','600']"
                                  data-width="['700','650','500','420']"
                                  data-height="none"
                                  data-whitespace="nowrap"
                                  data-transform_idle="o:1;"
                                  data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                                  data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                                  data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                                  data-start="1000" 
                                  data-splitin="none" 
                                  data-splitout="none" 
                                  data-responsive_offset="on" 
                                  style="z-index: 7; white-space: nowrap;"><a href="#" class="btn btn-dark btn-theme-colored2 btn-xl">Read More</a>
                                </div> -->
                                <!-- LAYER NR. 5 -->
                                <div class="tp-caption rs-parallaxlevel-0" 
                                  id="slide-2-layer-5" 
                                  data-x="['center','center','center','center']" data-hoffset="['310','33','0','0']" 
                                  data-y="['top','top','top','top']" data-voffset="['360','290','260','260']" 
                                  data-width="['700','650','600','420']"
                                  data-height="none"
                                  data-whitespace="nowrap"
                                  data-transform_idle="o:1;"
                                  data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
                                  data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                                  data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                                  data-mask_in="x:0px;y:0px;" 
                                  data-mask_out="x:0;y:0;" 
                                  data-start="1000" 
                                  data-splitin="none" 
                                  data-splitout="none" 
                                  data-responsive_offset="on" 
                                  data-responsive="off"
                                  style="z-index: 8; white-space: nowrap;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;">
                                </div>
                            </li>
                        </ul>
                        <div class="tp-bannertimer tp-bottom" style="height: 5px; background-color: rgba(166, 216, 236, 1.00);"></div>
                    </div>
                </div>
                <!-- END REVOLUTION SLIDER -->
            </div>
        </section>
        <!-- Section: Analysis -->
        <section id="analysis" class="">
            <div class="container pt-70">
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 text-center">
                            <h2 class="title font-42 line-bottom-double-line-centered  text-theme-colored">Doing the right thing,<span class="text-theme-colored2"> at the right time</span></h2>
                            <p class="">Established in 2013, BizStartDubai is assisting businesses enter, operate and grow in the fast-growing UAE market. We address clients including large multinationals, family owned enterprises and entrepreneurs on establishing corporate structures. We give special care and attention for start-ups.</p>
                            <p>Our solutions are bespoke according to each client’s specific need.</p>
                        </div>
                    </div>
                    <div class="row mt-40">
                        <div class="col-md-8 col-md-offset-2 text-center">
                            <h2 class="title font-42 line-bottom-double-line-centered  text-theme-colored">Business<span class="text-theme-colored2"> Setup</span></h2>
                        </div>
                        <div class="col-md-4">
                            <div class="icon-box icon-filled text-center">
                                <div class="image">
                                    <img src="<?= base_url('assets/images/mainland.jpg') ?>" class="img-responsive">
                                    <div class="caption">
                                        <h4>UAE MAINLAND</h4>
                                    </div>
                                    <div class="overlay-box">
                                        <div class="overlay-inner">
                                            <div class="overlay-content">
                                                <div class="text">Dubai Mainland is famous for its business friendly infrastructure and facilities.</div>
                                                <a href="<?= site_url('uae-mainland-business-setup') ?>" class="read-more">Read More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="icon-box icon-filled text-center">
                                <div class="image">
                                    <img src="<?= base_url('assets/images/freezone.jpg') ?>" class="img-responsive">
                                    <div class="caption">
                                        <h4>UAE FREEZONE</h4>
                                    </div>
                                    <div class="overlay-box">
                                        <div class="overlay-inner">
                                            <div class="overlay-content">
                                                <div class="text">A free zone is one of the most lucrative areas for a business setup in Dubai.</div>
                                                <a href="<?= site_url('business-setup-in-uae-freezone') ?>" class="read-more">Read More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <div class="col-md-4">
                            <div class="icon-box icon-filled text-center">
                                <div class="image">
                                    <img src="<?= base_url('assets/images/offshore.jpg') ?>" class="img-responsive">
                                    <div class="caption">
                                        <h4>OFFSHORE BUSINESSES</h4>
                                    </div>
                                    <div class="overlay-box">
                                        <div class="overlay-inner">
                                            <div class="overlay-content">
                                                <div class="text">An offshore company is flexible enough to have a mixture of features.</div>
                                                <a href="<?= site_url('offshore-business-setup-in-dubai') ?>" class="read-more">Read More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-10">
                        <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
                            <div class="funfact box-shadow2 text-center bg-white pb-30">
                                <div class="odometer-animate-number text-theme-colored font-weight-600 font-48" data-value="1950" data-theme="minimal">0</div>
                                <h4 class="text-theme-colored2 mt-0 mb-0">Happy Business Owners</h4>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
                            <div class="funfact box-shadow2 text-center bg-white pb-30">
                                <div class="odometer-animate-number text-theme-colored font-weight-600 font-48" data-value="620" data-theme="minimal">0</div>
                                <h4 class="text-theme-colored2 mt-0 mb-0">Branding Works</h4>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
                            <div class="funfact box-shadow2 text-center bg-white pb-30">
                                <div class="odometer-animate-number text-theme-colored font-weight-600 font-48" data-value="220" data-theme="minimal">0</div>
                                <h4 class="text-theme-colored2 mt-0 mb-0">Office Spaces</h4>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 mb-md-0">
                            <div class="funfact box-shadow2 text-center bg-white pb-30">
                                <div class="odometer-animate-number text-theme-colored font-weight-600 font-48" data-value="35" data-theme="minimal">0</div>
                                <h4 class="text-theme-colored2 mt-0 mb-0">Corporate Law Practices</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Section: about -->
        <section id="about" class="bg-silver-light">
            <div class="container pt-40">
                <div class="row">
                    <div class="col-md-6">
                        <h4 class="">We work as Partners, not as an Agency</h4>
                        <h2 class="font-42">Partner with the right team for the perfect start</h2>
                        <p>Bizstart is a Corporate Service Provider specialized in designing bespoke solutions for Business Setup, Finance Services, Accounting and Branding Solutions to individuals and companies who are willing to setup and grow their businesses in Dubai or entire UAE and all around the globe.</p>
                        <p>Since 2013, we have helped 1000s of Entrepreneurs & International Business Companies to set up their business in Dubai or anywhere in the UAE & penetrate new markets around the world through our Network.</p>
                        <a class="hvr-sweep-to-right btn btn-dark btn-theme-colored2 btn-lg mt-20 font-18" href="#">Register Now</a>
                    </div>
                    <div class="col-md-6">
                        <div class="">
                            <img alt="" src="<?= base_url('assets/images/about/tr3.png') ?>" class="img-responsive img-fullwidth">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Divider: Divider -->
        <section class="divider cloud-img" data-bg-img="<?= base_url('assets/images/bg/team-bg1.png') ?>">
            <div class="container pt-70 pb-130">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12 text-center">
                            <h2 class="text-theme-colored2 font-42">For Emergency Consulting Please Contact</h2>
                            <h3 class="font-42 text-white">(+971 58 908 5535)</h3>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Section: Services -->
        <section id="services" class="">
            <div class="container">
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="box-hover-effect play-button">
                                <div class="effect-wrapper">
                                    <div class="thumb">
                                        <img class="img-fullwidth" src="<?= base_url('assets/images/services/1.jpg') ?>" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h4 class="">Your search for a reliable and responsible management consulting ends here.</h4>
                            <h3 class="font-38">We provide premium consulting and professional services.</h3>
                            <a class="hvr-sweep-to-right btn btn-dark btn-theme-colored2 btn-lg mt-20 font-18" href="#">Choose Our Services</a>
                        </div>
                    </div>
                    <div class="row mt-40">
                        <div class="col-xs-12 col-sm-6 col-md-3 mb-sm-30">
                            <div class="box-hover-effect">
                                <div class="effect-wrapper">
                                    <div class="thumb">
                                        <img class="img-fullwidth" alt="" src="<?= base_url('assets/images/services/s1.jpg') ?>">
                                    </div>
                                    <h3>PRO Services</h3>
                                    <p>We will help you with all the formalities associated with setting up a company in UAE. Our PRO services include:</p>
                                    <ul>
                                      <li>Company registration</li>
                                      <li>Offering a Local Sponsor</li>
                                      <li>All type of VISAS formalities</li>
                                      <li>Assistance for all government work</li>
                                      <li>Legal translation</li>
                                      <li>End-to-end paperworks</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 mb-sm-30">
                            <div class="box-hover-effect">
                                <div class="effect-wrapper">
                                    <div class="thumb">
                                        <img class="img-fullwidth" alt="" src="<?= base_url('assets/images/services/s2.jpg') ?>">
                                    </div>
                                    <h3>Business Consulting</h3>
                                    <p>Once the license is setup, we can help with various business services. Our business services includes:</p>
                                    <ul>
                                      <li>Business support</li>
                                      <li>Insurance advisory</li>
                                      <li>Accounting Book-keeping</li>
                                      <li>HR Services</li>
                                      <li>Consulate services</li>
                                      <li>Certificate attestation</li>
                                      <li>PR & Media</li>
                                      <li>Business Loan assistance</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 mb-sm-30">
                            <div class="box-hover-effect">
                                <div class="effect-wrapper">
                                    <div class="thumb">
                                        <img class="img-fullwidth" alt="" src="<?= base_url('assets/images/services/s3.jpg') ?>">
                                    </div>
                                    <h3>Branding & Digital Marketing</h3>
                                    <ul>
                                      <li>Standard Branding Exercises</li>
                                      <li>Offline & Online Branding</li>
                                      <li>Office Branding & Designing</li>
                                      <li>Website Design & Development</li>
                                      <li>SMO, SEO, SEM</li>
                                      <li>Mobile Apps</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="box-hover-effect">
                                <div class="effect-wrapper">
                                    <div class="thumb">
                                        <img class="img-fullwidth" alt="" src="<?= base_url('assets/images/services/s4.jpg') ?>">
                                    </div>
                                    <h3>Accounting & VAT</h3>
                                    <p>Our experienced accountants and bookkeepers offers a wide range of accounting services in UAE that provides answers to the financial needs to any business.</p>
                                    <ul>
                                      <li>Accounting</li>
                                      <li>Bookkeeping</li>
                                      <li>VAT registration & advisory</li>
                                      <li>Taxation</li>
                                      <li>CFO services</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </section>
        <!--start Team Section-->
        <!-- <section id="team" class="" data-bg-img="images/bg/r2.png">
            <div class="container pt-90">
                <div class="section-title text-center">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <h2 class="mt-0 line-height-1 line-bottom-double-line-centered text-theme-colored">Team <span class="text-theme-colored2">Members</span></h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem<br> voluptatem obcaecati!</p>
                        </div>
                    </div>
                </div>
                <div class="section-content">
                    <div class="row">
                        <div class="owl-carousel-3col">
                            <div class="item">
                                <div class="box-hover-effect text-center">
                                    <div class="effect-wrapper">
                                        <div class="thumb">
                                            <img class="img-circle img-thumbnail mb-15" src="images/team/1.jpg" alt="">
                                        </div>
                                        <div class="">
                                            <h3 class="font-30"><a href="#">Sakib Martin</a></h3>
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing Molestias non nulla placeat odio dicta alias</p>
                                            <ul class="styled-icons icon-dark icon-theme-colored2 icon-circled icon-sm">
                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                            </ul>
                                        </div>                
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="box-hover-effect text-center">
                                    <div class="effect-wrapper">
                                        <div class="thumb">
                                            <img class="img-circle img-thumbnail mb-15" src="images/team/2.jpg" alt="">
                                        </div>
                                        <div class="">
                                            <h3 class="font-30"><a href="#">Maria Mikel</a></h3>
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing Molestias non nulla placeat odio dicta alias</p>
                                            <ul class="styled-icons icon-dark icon-theme-colored2 icon-circled icon-sm">
                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                            </ul>
                                        </div>                
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="box-hover-effect text-center">
                                    <div class="effect-wrapper">
                                        <div class="thumb">
                                            <img class="img-circle img-thumbnail mb-15" src="images/team/3.jpg" alt="">
                                        </div>
                                        <div class="">
                                            <h3 class="font-30"><a href="#">Ismail Jone</a></h3>
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing Molestias non nulla placeat odio dicta alias</p>
                                            <ul class="styled-icons icon-dark icon-theme-colored2 icon-circled icon-sm">
                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                            </ul>
                                        </div>                
                                    </div>
                                </div>
                            </div>              
                        </div>
                    </div>
                </div>
            </div>
        </section> -->
        <!--start Pricing Section-->
        <section id="pricing" class="bg-silver-light">
            <div class="container pt-30">
                <div class="section-title text-center">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <h2 class="mt-0 line-height-1 line-bottom-double-line-centered text-theme-colored font-42">Tailor-Made <span class="text-theme-colored2">Packages</span></h2>
                            <p>Check our tailor-made packages which suits best for you. We deliver strategies according to your business needs.</p>
                        </div>
                    </div>
                </div>
                <div class="section-content">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-4 hvr-float-shadow mb-sm-30">
                            <div class="pricing text-center bg-white">
                                <div class="p-10">
                                    <h3>Freezone</h3>
                                </div>
                                <div class="bg-theme-colored2 text-center p-10">
                                    <div class="font-36 text-white pt-0">AED 5000 <span class="font-18">onwards</span></div>
                                </div>
                                <div class="pricing-options">
                                    <ul class="">
                                        <li>100% Ownership</li>
                                        <li>100% repatriation of profits</li>
                                        <li>Multiple license options</li>
                                    </ul>
                                </div>                
                                <a class="btn btn-lg btn-theme-colored btn-block p-20 font-20 btn-flat" href="#">Sign Up</a>           
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 hvr-float-shadow mb-sm-30">
                            <div class="pricing text-center bg-white">
                                <div class="p-10">
                                    <h3>Dubai Mainland</h3>
                                </div>
                                <div class="bg-theme-colored2 text-center p-10">
                                    <div class="font-36 text-white pt-0">AED 25000 <span class="font-18">onwards</span></div>
                                </div>
                                <div class="pricing-options">
                                    <ul>
                                        <li>Start your business in 24 hours</li>
                                        <li>Ease of doing business with the rest of UAE</li>
                                        <li>Access to global marketplace</li> 
                                    </ul>
                                </div>                
                                <a class="btn btn-lg btn-theme-colored btn-block p-20 font-20 btn-flat" href="#">Sign Up</a>           
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 hvr-float-shadow mb-sm-30">
                            <div class="pricing text-center bg-white">
                                <div class="p-10">
                                    <h3>Offshore</h3>
                                </div>
                                <div class="bg-theme-colored2 text-center p-10">
                                    <div class="font-36 text-white pt-0">AED 5000<span class="font-18"> onwards</span></div>
                                </div>
                                <div class="pricing-options">
                                    <ul>
                                        <li>No minimum capital requirement</li>
                                        <li>Reputed offshore destinations</li>
                                        <li>Swift incorporation</li>
                                    </ul>
                                </div>                
                                <a class="btn btn-lg btn-theme-colored btn-block p-20 font-20 btn-flat" href="#">Sign Up</a>           
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Section: -->
        <!-- <section class="divider" data-bg-img="images/bg/team-bg1.png" >
            <div class="container pt-100 pb-100">
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-4">
                            <h2 class="text-white mt-50">Happy Client's Say Something Of Our Consulting & Business</h2>
                        </div>
                        <div class="col-md-8">
                            <div class="owl-carousel-1col pr-50 pl-50 pr-sm-50 pl-sm-50 pb-40 border-2px border-theme-colored2 bg-theme-colored-transparent-7">
                                <div class="item">
                                    <div class="testimonial-wrapper text-center">
                                        <div class="content mt-30">
                                            <i class="fa fa-quote-left text-theme-colored2 font-42 mt-15 mb-10 mb-sm-0"></i>
                                            <a class="mt-15 mb-15 display-block" href="#">
                                                <img alt="" src="images/testimonials/1.jpg" class="img-circle img-thumbnail">
                                            </a>
                                            <p class="mb-sm-10 lead text-white">Lorem Ipsum has been the industry's standard dummy text ever, when an took a galley of type and it to make a type book.It has survived into</p>
                                            <h3 class="text-theme-colored2">Corvin Adams</h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="testimonial-wrapper text-center">
                                        <div class="content mt-30">
                                            <i class="fa fa-quote-left text-theme-colored2 font-42 mt-15 mb-10 mb-sm-0"></i>
                                            <a class="mt-15 mb-15 display-block" href="#">
                                                <img alt="" src="images/testimonials/2.jpg" class="img-circle img-thumbnail">
                                            </a>
                                            <p class="mb-sm-10 lead text-white">Lorem Ipsum has been the industry's standard dummy text ever, when an took a galley of type and it to make a type book.It has survived into</p>
                                            <h3 class="text-theme-colored2">Corvin Adams</h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="testimonial-wrapper text-center">
                                        <div class="content mt-30">
                                            <i class="fa fa-quote-left text-theme-colored2 font-42 mt-15 mb-10 mb-sm-0"></i>
                                            <a class="mt-15 mb-15 display-block" href="#">
                                                <img alt="" src="images/testimonials/3.jpg" class="img-circle img-thumbnail">
                                            </a>
                                            <p class="mb-sm-10 lead text-white">Lorem Ipsum has been the industry's standard dummy text ever, when an took a galley of type and it to make a type book.It has survived into</p>
                                            <h3 class="text-theme-colored2">Corvin Adams</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->
      
        <!--start blog Section-->
        <!-- <section id="blog" class="bg-silver-light">
            <div class="container pt-90">
                <div class="section-title text-center">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <h2 class="mt-0 line-height-1 line-bottom-double-line-centered">Lates <span class="text-theme-colored2">News</span></h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem<br> voluptatem obcaecati!</p>
                        </div>
                    </div>
                </div>
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="owl-carousel-3col">
                                <div class="item">
                                    <article class="post hover-box-shadow">
                                        <div class="post-thumb position-relative">
                                            <img src="images/blog/1.jpg" class="img-fullwidth" alt="">
                                            <span>26 October 2019</span>
                                        </div>
                                        <div class="post-description bg-white border-1px p-20">
                                            <h3 class="post-title mt-0 mb-15">Lates Consulting Post</h3>
                                            <div class="pb-10">
                                                <span class="mb-10 text-theme-colored mr-10 font-15"><i class="fa fa-commenting-o mr-5 "></i> 214 Comments</span>
                                                <span class="mb-10 text-theme-colored mr-10 font-15"><i class="fa fa-heart-o mr-5 "></i> 895 Likes</span>
                                            </div>
                                            <p class="mb-10">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo provident neque deleniti eos dicta possimus dolores</p>
                                            <a href="#" class="blog-read-more text-theme-colored2 font-17">Read More →</a>
                                        </div>
                                    </article>
                                </div>
                                <div class="item">
                                    <article class="post hover-box-shadow">
                                        <div class="post-thumb position-relative">
                                            <img src="images/blog/2.jpg" class="img-fullwidth" alt="">
                                            <span>26 October 2020</span>
                                        </div>
                                        <div class="post-description bg-white border-1px p-20">
                                            <h3 class="post-title mt-0 mb-15">Lates Consulting Post</h3>
                                            <div class="pb-10">
                                                <span class="mb-10 text-theme-colored mr-10 font-15"><i class="fa fa-commenting-o mr-5 "></i> 214 Comments</span>
                                                <span class="mb-10 text-theme-colored mr-10 font-15"><i class="fa fa-heart-o mr-5 "></i> 895 Likes</span>
                                            </div>
                                            <p class="mb-10">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo provident neque deleniti eos dicta possimus dolores</p>
                                            <a href="#" class="blog-read-more text-theme-colored2 font-17">Read More →</a>
                                        </div>
                                    </article>
                                </div>
                                <div class="item">
                                    <article class="post hover-box-shadow">
                                        <div class="post-thumb position-relative">
                                            <img src="images/blog/3.jpg" class="img-fullwidth" alt="">
                                            <span>26 October 2021</span>
                                        </div>
                                        <div class="post-description bg-white border-1px p-20">
                                            <h3 class="post-title mt-0 mb-15">Lates Consulting Post</h3>
                                            <div class="pb-10">
                                                <span class="mb-10 text-theme-colored mr-10 font-15"><i class="fa fa-commenting-o mr-5 "></i> 214 Comments</span>
                                                <span class="mb-10 text-theme-colored mr-10 font-15"><i class="fa fa-heart-o mr-5 "></i> 895 Likes</span>
                                            </div>
                                            <p class="mb-10">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo provident neque deleniti eos dicta possimus dolores</p>
                                            <a href="#" class="blog-read-more text-theme-colored2 font-17">Read More →</a>
                                        </div>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->

        <!-- Divider: Contact -->
        <!-- <section id="contact" class="" data-bg-img="images/bg/r2.png">
            <div class="container pt-90 pb-60">
                <div class="section-title text-center">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <h2 class="mt-0 line-height-1 line-bottom-double-line-centered">Contact <span class="text-theme-colored2">Now</span></h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem<br> voluptatem obcaecati!</p>
                        </div>
                    </div>
                </div>
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="">
                                <img alt="" src="images/about/tr2.png" class="img-responsive img-fullwidth">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <form id="contact_form" name="contact_form" class="" action="" method="post">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input name="form_name" class="form-control p-30" type="text" placeholder="Enter Name" required="">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input name="form_email" class="form-control required email p-30" type="email" placeholder="Enter Email">
                                        </div>
                                    </div>  
                                </div>                  
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input name="form_subject" class="form-control required p-30" type="text" placeholder="Enter Subject">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input name="form_phone" class="form-control p-30" type="text" placeholder="Enter Phone">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <textarea name="form_message" class="form-control required p-30" rows="4" placeholder="Enter Message"></textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="hvr-sweep-to-right btn btn-dark btn-theme-colored btn-lg mt-10 font-18" data-loading-text="Please wait...">Send your message</button>
                                    <button type="reset" class="hvr-sweep-to-right btn btn-dark btn-theme-colored2 btn-lg mt-10 font-18">Reset</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->
    </div>

  