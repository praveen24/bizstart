<div class="container-fluid">
	<div class="row breadcrumb-section">
		<div class="container">
			<div class="row">
				<div class="col-sm-10">
					<ul class="breadcrumb">
					    <li><a href="<?=site_url()?>">Home</a></li>
					    <li class="active">About Bizstart</li>
					</ul>
				</div>
				<div class="col-sm-2">
					<div class="pull-right sm-pull-none mb-sm-15">
                        <button class="hvr-sweep-to-right btn btn-dark btn-theme-colored2 font-14" data-target="#signupModal" data-toggle="modal">SIGN UP</button>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row pt-50 pb-40">
		<div class="col-sm-6">
			<img src="<?= base_url('assets/images/1.jpg') ?>" class="img-responsive">
		</div>
		<div class="col-sm-6">
			<h3 class="mt-0">Bizstart Dubai</h3>
			<p class="text-justify">Bizstart Dubai is a Corporate Service Provider specialized in designing bespoke solutions for Business Setup, Finance Services, Accounting and Branding Solutions to individuals and companies who are willing to setup and grow their businesses in Dubai or entire UAE and all around the globe.</p>
			<p class="text-justify">Since 2013, we have helped 1000s of Entrepreneurs & International Business Companies to set up their business in Dubai or anywhere in the UAE & penetrate new markets around the world through our Network.</p>
			<p class="text-justify">BizStart Dubai is assisting businesses enter, operate and grow in the fast-growing UAE market. We address clients including large multinationals, family owned enterprises and entrepreneurs on establishing corporate structures. We give special care and attention for start-ups.</p>
			<p class="text-justify">
				Our solutions are bespoke according to each client's specific need.
			</p>
		</div>
	</div>
</div>