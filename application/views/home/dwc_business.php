<div class="container-fluid">
	<div class="row breadcrumb-section">
		<div class="container">
			<div class="row">
				<div class="col-sm-10">
					<ul class="breadcrumb">
					    <li><a href="<?=site_url()?>">Home</a></li>
					    <li><a href="#">Business Setup in UAE</a></li>
					    <li class="active">Business Setup in DWC</li>
					</ul>
				</div>
				<div class="col-sm-2">
					<div class="pull-right sm-pull-none mb-sm-15">
                        <button class="hvr-sweep-to-right btn btn-dark btn-theme-colored2 font-14" data-target="#signupModal" data-toggle="modal">SIGN UP</button>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row pt-50 pb-40">
		<div class="col-sm-12">
			<h2>Business set up in DWC</h2>
			<p class="text-justify">DWC OR Dubai South is a registered trademark of Dubai World Central Corporation. DUBAI WORLD CENTRAL (DWC) is comprised of eight districts: logistics, aviation (incuding Al Maktoum International Airport), humanitarian, residential, commercial, leisure, exhibition and commercial. Dubai Logistics City and Aviation City is concerned for the purpose of company formations. The licenses that can be obtained in both these free zones are identical, save that businesses registering must be relevant to the free zone in question.</p>
			<strong>Benefits of DWC</strong>
			<ul class="ul_listing">
				<li>Free Zone environment</li>
				<li>State of the art facilities</li>
				<li>100% tax free</li>
				<li>100% foreign ownership</li>
				<li>No corporate tax</li>
				<li>Variety of license activities</li>
				<li>Flexible commercial lease terms</li>
				<li>Competitive rates</li>
				<li>Fitted – out and core shell offices</li>
				<li>Serviced desks & offices for SME’s and start up</li>
				<li>Direct access to Al Maktoum International Airport</li>
			</ul>
		</div>
	</div>
</div>