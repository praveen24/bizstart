<div class="container-fluid">
	<div class="row breadcrumb-section">
		<div class="container">
			<div class="row">
				<div class="col-sm-10">
					<ul class="breadcrumb">
					    <li><a href="<?=site_url()?>">Home</a></li>
					    <li><a href="#">Business Setup in UAE</a></li>
					    <li class="active">Business Setup in Jabel Ali</li>
					</ul>
				</div>
				<div class="col-sm-2">
					<div class="pull-right sm-pull-none mb-sm-15">
                        <button class="hvr-sweep-to-right btn btn-dark btn-theme-colored2 font-14" data-target="#signupModal" data-toggle="modal">SIGN UP</button>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row pt-50 pb-40">
		<div class="col-sm-12">
			<h2>Business set up in Jabel Ali</h2>
			<p class="text-justify">The Jebel Ali Free Zone Authority (JAFZA) is one of the best business hubs to incorporate in the world.</p>
			<p class="text-justify">Depending on the number of shareholders and the nature of business, Jafza offers three formation types, three types of licenses and a wide variety of license activities. There are mainly three range of licenses in JAFZA:</p>
			<ul class="ul_listing">
				<li>Multiple shareholders</li>
				<li>A single shareholder</li>
				<li>Any existing established entity</li>
			</ul>
		</div>
	</div>
</div>