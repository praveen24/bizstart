<div class="container-fluid">
	<div class="row breadcrumb-section">
		<div class="container">
			<div class="row">
				<div class="col-sm-10">
					<ul class="breadcrumb">
					    <li><a href="<?=site_url()?>">Home</a></li>
					    <li><a href="#">Start a Company</a></li>
					    <li><a href="#">Where to start a company in the UAE?</a></li>
					    <li class="active">UAE Freezone</li>
					</ul>
				</div>
				<div class="col-sm-2">
					<div class="pull-right sm-pull-none mb-sm-15">
                        <button class="hvr-sweep-to-right btn btn-dark btn-theme-colored2 font-14" data-target="#signupModal" data-toggle="modal">SIGN UP</button>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row pt-50 pb-40">
		<div class="col-sm-12">
			<h2>UAE Freezone Business Setup</h2>
			<p class="text-justify">Freezones in the UAE offer entrepreneurs who are willing to have their own company this opportunity without the need for a local sponsor. </p>
			<p class="text-justify">This type of company formation is designed to encourage foreign investment with the easiest start-up processes as well as lower overhead cost. </p>
			<p class="text-justify">We offer the flexibility to provide our clients with the exact package that suits your needs, as there are various Free zones all over UAE, we assist you to get the license from the best free zone as per your business requirements from the early first step till the last, we take care of everything so that you can focus on your business plans.</p>
			<!-- <ul class="list_ul">
				<li>
					<p>100% ownership of your company</p>
				</li>
				<li>
					<p>Zero taxation</p>
				</li>
				<li><p>No requirement to share 51% of your company with a local sponsor</p></li>
				<li><p>No customs duty for import/export</p></li>
				<li><p>Shareholders and the employees are eligible for getting UAE residency visas</p></li>
				<li>
					<p>25 years lease options, warehouse facilities, availability of areas for production and assembling</p>
				</li>
				<li>
					<p>25 years lease options, warehouse facilities, availability of areas for production and assembling</p>
				</li>
			</ul> -->
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3613.180226136265!2d55.17074981448218!3d25.095759841967947!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f6b7af1ca6c7d%3A0xb992366792daa99c!2sDAMAC+Executive+Heights!5e0!3m2!1sen!2sin!4v1540797742074" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
</div>