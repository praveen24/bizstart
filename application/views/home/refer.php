<div class="container-fluid">
	<div class="row breadcrumb-section">
		<div class="container">
			<div class="row">
				<div class="col-sm-10">
					<ul class="breadcrumb">
					    <li><a href="<?=site_url()?>">Home</a></li>
					    <li class="active">Refer and Earn</li>
					</ul>
				</div>
				<div class="col-sm-2">
					<div class="pull-right sm-pull-none mb-sm-15">
                        <button class="hvr-sweep-to-right btn btn-dark btn-theme-colored2 font-14" data-target="#signupModal" data-toggle="modal">SIGN UP</button>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row pt-50 pb-40">
		<div class="col-sm-8 col-sm-offset-2">
			<h3 class="text-center">Are your friends or family looking to start a company in UAE?</h3>
			<h4 class="text-center">Refer them to BizstartDubai and we will reward you with AED 3,000 on each referring successful clients.</h4>
			<form action="" method="post">
				<div class="refer-section">
					<div class="col-sm-12 form-group">
						<label>Your Name</label>
						<input type="text" name="name" class="form-control">
					</div>
					<div class="col-sm-12 form-group">
						<label>Your Email Id</label>
						<input type="email" name="email" class="form-control">
					</div>
					<div class="col-sm-12 form-group">
						<label>Your Phone Number</label>
						<input type="text" name="phone" class="form-control">
					</div>
					<div class="col-sm-12 form-group">
						<label>Your Referrals Name</label>
						<input type="text" name="referral_name" class="form-control">
					</div>
					<div class="col-sm-12 form-group">
						<label>Your Referrals Email Id</label>
						<input type="text" name="referral_email" class="form-control">
					</div>
					<div class="col-sm-12 form-group">
						<label>Your Referrals Contact Number</label>
						<input type="text" name="referral_contact" class="form-control">
					</div>
					<div class="col-sm-12 form-group">
						<label>Do They Know We Will Contact Them?</label>
						<input type="radio" name="know" value="Yes" id="contactYes">
						<label for="contactYes">Yes</label>
						<input type="radio" name="know" value="No" id="contactNo">
						<label for="contactNo">No</label>
					</div>
					<div class="col-sm-12 form-group">
						<button class="hvr-sweep-to-right btn btn-dark btn-theme-colored2 btn-lg font-18 pull-right">Submit</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>