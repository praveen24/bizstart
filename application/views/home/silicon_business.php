<div class="container-fluid">
	<div class="row breadcrumb-section">
		<div class="container">
			<div class="row">
				<div class="col-sm-10">
					<ul class="breadcrumb">
					    <li><a href="<?=site_url()?>">Home</a></li>
					    <li><a href="#">Business Setup in UAE</a></li>
					    <li class="active">Business Setup in Silicon Oasis</li>
					</ul>
				</div>
				<div class="col-sm-2">
					<div class="pull-right sm-pull-none mb-sm-15">
                        <button class="hvr-sweep-to-right btn btn-dark btn-theme-colored2 font-14" data-target="#signupModal" data-toggle="modal">SIGN UP</button>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row pt-50 pb-40">
		<div class="col-sm-12">
			<h2>Business set up in Silicon Oasis</h2>
			<p class="text-justify">Owned by the government of Dubai, Dubai Silicon Oasis Authority is a technology park offering world-class infrastructure and many business incentives to firms operating within the free zone.</p>
			<p class="text-justify">Depending on the number of shareholders and the nature of business, Jafza offers three formation types, three types of licenses and a wide variety of license activities. There are mainly three range of licenses in JAFZA:</p>
			<strong>Benefits of registering a company in Dubai Silicon Oasis</strong>
			<ul class="ul_listing" style="margin-top: 15px;">
				<li>100% foreign ownership </li>
				<li>100% repatriation on capital and profits</li>
				<li>Fast-track Silicon Oasis business setup and licensing</li>
				<li>100% tax exemption on Corporate and personal income</li>
				<li>100% tax exemption on All imports and exports.</li>
			</ul>
		</div>
	</div>
</div>