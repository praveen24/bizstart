    <footer id="footer" class="footer" data-bg-img="<?= base_url('assets/images/footer-bg.png') ?>" data-bg-color="#152029">
        <div class="container pt-40">
            <div class="row border-bottom-black">
                <div class="col-sm-6 col-md-3">
                    <div class="widget dark">
                        <img src="<?= base_url('assets/images/footer_logo.jpg') ?>" class="img-responsive">
                        <div class="clearfix"></div><br>
                        <h3 class="widget-title mb-10 font-16">BizStartDubai Management Consultancy</h3>
                        <p class="text-white">Established on 2013, BizStartDubai is assisting businesses enter, operate and grow in the fast-growing UAE market.</p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="widget dark">
                        <h3 class="widget-title mb-10">Start a Company</h3>
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <ul class="list-border">
                                    <li><a href="#" class="text-white">Business Setup in Dubai</a></li>
                                    <li><a href="#" class="text-white">DED Business Setup</a></li>
                                    <li><a href="#" class="text-white">DWC Business Setup</a></li>
                                    <li><a href="#" class="text-white">Business Setup in DMCC</a></li>
                                    <li><a href="#" class="text-white">Business Setup in Abudhabi</a></li>
                                    <li><a href="#" class="text-white">Business Setup in Fujairah</a></li>
                                    <li><a href="#" class="text-white">Business Setup in Ajman</a></li>
                                    <li><a href="#" class="text-white">Business Setup in Silicon Oasis</a></li>
                                    <li><a href="#" class="text-white">Business Setup in Sharjah</a></li>
                                    <li><a href="#" class="text-white">Business Setup in Ras Al Khaimah</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="widget dark">
                        <h3 class="widget-title mb-10">Our Services</h3>
                        <div class="row clearfix">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <ul class="list-border">
                                    <li><a href="#" class="text-white">Pro & Visa Services</a></li>
                                    <li><a href="#" class="text-white">Paperwork & Translation</a></li>
                                    <li><a href="#" class="text-white">Trademark Registration</a></li>
                                    <li><a href="#" class="text-white">Branding</a></li>
                                    <li><a href="#" class="text-white">Website Design & Development</a></li>
                                    <li><a href="#" class="text-white">Digital Marketing in UAE</a></li>
                                    <li><a href="#" class="text-white">VAT Book Keeping & Accounting</a></li>
                                    <li><a href="#" class="text-white">Business Consulting in UAE</a></li>
                                    <li><a href="#" class="text-white">Market Analysis in UAE</a></li>
                                    <li><a href="#" class="text-white">Corporate Lawyers and Law Practices</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="widget dark">
                        <h3 class="widget-title mb-10">Contact Us</h3>
                        <ul class="list-inline mt-5">
                            <li class="m-0 pl-10 pr-10"> <i class="fa fa-map-marker text-theme-colored2 mr-5"></i> <a class="text-white" href="#">P.O.Box 18938, 12A (06), Damac Executive Heights Dubai, UAE</a> </li>
                            <li class="m-0 pl-10 pr-10"> <i class="fa fa-mobile text-theme-colored2 mr-5"></i> <a class="text-white" href="#">+971 58 908 5535</a> </li>
                            <li class="m-0 pl-10 pr-10"> <i class="fa fa-mobile text-theme-colored2 mr-5"></i> <a class="text-white" href="#">+971 4  239 4771</a> </li>
                            <li class="m-0 pl-10 pr-10"> <i class="fa fa-envelope-o text-theme-colored2 mr-5"></i> <a class="text-white" href="#">connect@bizstartdubai.com</a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom bg-theme-colored2">
            <div class="container pt-10 pb-0">
                <div class="row">
                    <div class="col-md-6 sm-text-center">
                        <p class="text-white m-0">Copyright &copy;2018 ThemeMascot. All Rights Reserved</p>
                    </div>
                    <div class="col-md-6 text-right flip sm-text-center">
                        <div class="widget no-border m-0">
                            <ul class="styled-icons icon-dark icon-theme-colored icon-circled icon-sm">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end wrapper -->
<!--   Appoinment modal  -->
<div id="appointmentModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">GET FREE CONSULTANCY</h4>
            </div>
            <div class="modal-body">
                <form action="<?= site_url('get_appoinment') ?>" method="post">
                    <div class="row">
                        <div class="col-md-12">
                            <strong>Note:</strong>fields marked with (<span class="text-danger">*</span>) are mandatory
                        </div>
                        <div class="clearfix"></div><br>
                        <div class="form-group col-sm-12">
                            <label class="text-danger pull-right error name_error" for="name"></label>
                            <input type="text" name="name" placeholder="Name" class="form-control" id="name">
                            <span class="star">*</span>
                        </div>
                        <div class="form-group col-sm-6">
                            <label class="text-danger pull-right error mobile_error" for="mobile"></label>
                            <input type="text" name="mobile" class="form-control" id="mobile" placeholder="Mobile No." pattern="^\d{10}$">
                            <span class="star">*</span>
                        </div>
                        <div class="col-sm-6 form-group">
                            <label class="text-danger pull-right error email_error" for="email"></label>
                            <input type="email" name="email" placeholder="Email" id="email" class="form-control">
                            <span class="star">*</span>
                        </div>
                        <div class="form-group col-sm-12">
                            <textarea class="form-control" name="message" rows="4" placeholder="Your Message"></textarea>
                        </div>
                        <div class="form-group col-sm-12">
                            <button id="appointment_btn" class="hvr-sweep-to-right btn btn-dark btn-theme-colored2 pull-right">BOOK AN APPOINMENT</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="signupModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Sign Up</h4>
            </div>
            <div class="modal-body">
                <h5 class="text-center">One of our business setup consultants will get back to you within 24 working hours.</h5>
                <form action="<?= site_url('signup') ?>" method="post">
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <input type="text" name="name" placeholder="Name" class="form-control">
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="email" name="email" placeholder="Email" class="form-control">
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="text" name="mobile" placeholder="Phone" class="form-control">
                        </div>
                        <div class="col-md-12 form-group">
                            <textarea rows="4" class="form-control" placeholder="Message" name="message"></textarea>
                        </div>
                        <div class="form-group col-md-12">
                            <button class="hvr-sweep-to-right btn btn-dark btn-theme-colored2 font-18 pull-right">SEND</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--   End -->
<!-- Footer Scripts -->

<!-- ext  ernal javascripts -->
<script src="<?= base_url('assets/js/jquery-2.2.4.min.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery-ui.min.js') ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
<!-- JS | jquery plugin collection for this theme -->
<script src="<?= base_url('assets/js/jquery-plugin-collection.js') ?>"></script>
<!-- JS | Custom script for all pages -->
<script src="<?= base_url('assets/js/custom.js') ?>"></script>
<script src="<?= base_url('assets/js/extra.js') ?>"></script>

<!-- Revolution Slider 5.x SCRIPTS -->
<script src="<?= base_url('assets/js/revolution-slider/js/jquery.themepunch.tools.min.js')?>"></script>
<script src="<?= base_url('assets/js/revolution-slider/js/jquery.themepunch.revolution.min.js') ?>"></script>
<script src="<?= base_url('assets/js/extra-rev-slider.js') ?>"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
      (Load Extensions only on Local File Systems ! 
       The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="<?= base_url('assets/js/revolution-slider/js/extensions/revolution.extension.actions.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/revolution-slider/js/extensions/revolution.extension.carousel.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/revolution-slider/js/extensions/revolution.extension.kenburn.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/revolution-slider/js/extensions/revolution.extension.migration.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/revolution-slider/js/extensions/revolution.extension.parallax.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/revolution-slider/js/extensions/revolution.extension.slideanims.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/revolution-slider/js/extensions/revolution.extension.video.min.js') ?>"></script>

</body>

</html>