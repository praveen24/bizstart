<!DOCTYPE html>
<html dir="ltr" lang="en">
	<head>
		<!-- Meta Tags -->
		<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
		<meta name="description" content="KonsultPro | Business Consulting & Corporate Finance HTML5 Template" />
		<meta name="keywords" content="consulting, business, corporate, finance, layers, doctor" />
		<meta name="author" content="ThemeMascot" />

		<!-- Page Title -->
		<title>BizStart | Business Setup in UAE</title>

		<!-- Favicon and Touch Icons -->
		<link href="<?= base_url('assets/images/favicon.png') ?>" rel="shortcut icon" type="image/png">
		<link href="<?= base_url('assets/images/apple-touch-icon.png') ?>" rel="apple-touch-icon">
		<link href="<?= base_url('assets/images/apple-touch-icon-72x72.png') ?>" rel="apple-touch-icon" sizes="72x72">
		<link href="<?= base_url('assets/images/apple-touch-icon-114x114.png') ?>" rel="apple-touch-icon" sizes="114x114">
		<link href="<?= base_url('assets/images/apple-touch-icon-144x144.png') ?>" rel="apple-touch-icon" sizes="144x144">

		<!-- Stylesheet -->
		<link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css">
		<link href="<?= base_url('assets/css/jquery-ui.min.css') ?>" rel="stylesheet" type="text/css">
		<link href="<?= base_url('assets/css/animate.css') ?>" rel="stylesheet" type="text/css">
		<link href="<?= base_url('assets/css/css-plugin-collections.css') ?>" rel="stylesheet"/>
		<!-- CSS | menuzord megamenu skins -->
		<link href="<?= base_url('assets/css/menuzord-megamenu.css') ?>" rel="stylesheet"/>
		<link id="menuzord-menu-skins" href="<?= base_url('assets/css/menuzord-skins/menuzord-top-bottom-boxed-border.css') ?>" rel="stylesheet"/>
		<!-- CSS | Main style file -->
		<link href="<?= base_url('assets/css/style-main.css') ?>" rel="stylesheet" type="text/css">
		<!-- CSS | Preloader Styles -->
		<link href="<?= base_url('assets/css/preloader.css') ?>" rel="stylesheet" type="text/css">
		<!-- CSS | Custom Margin Padding Collection -->
		<link href="<?= base_url('assets/css/custom-bootstrap-margin-padding.css') ?>" rel="stylesheet" type="text/css">
		<!-- CSS | Responsive media queries -->
		<link href="<?= base_url('assets/css/responsive.css') ?>" rel="stylesheet" type="text/css">
		<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
		<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->

		<!-- Revolution Slider 5.x CSS settings -->
		<link  href="<?= base_url('assets/js/revolution-slider/css/settings.css') ?>" rel="stylesheet" type="text/css"/>
		<link  href="<?= base_url('assets/js/revolution-slider/css/layers.css') ?>" rel="stylesheet" type="text/css"/>
		<link  href="<?= base_url('assets/js/revolution-slider/css/navigation.css') ?>" rel="stylesheet" type="text/css"/>

		<!-- CSS | Theme Color -->
		<link href="<?= base_url('assets/css/colors/theme-skin-color-set3.css') ?>" rel="stylesheet" type="text/css">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<!--Start of Tawk.to Script-->
		<script type="text/javascript">
			var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
			(function(){
				var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
				s1.async=true;
				s1.src='https://embed.tawk.to/5bd7f6ea476c2f239ff6890a/default';
				s1.charset='UTF-8';
				s1.setAttribute('crossorigin','*');
				s0.parentNode.insertBefore(s1,s0);
			})();
		</script>
		<!--End of Tawk.to Script-->
	</head>
	<body class="">