<div class="container-fluid">
	<div class="row breadcrumb-section">
		<div class="container">
			<div class="row">
				<div class="col-sm-10">
					<ul class="breadcrumb">
					    <li><a href="<?=site_url()?>">Home</a></li>
					    <li class="active">Co-working Spaces</li>
					</ul>
				</div>
				<div class="col-sm-2">
					<div class="pull-right sm-pull-none mb-sm-15">
                        <button class="hvr-sweep-to-right btn btn-dark btn-theme-colored2 font-14" data-target="#signupModal" data-toggle="modal">SIGN UP</button>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row pt-50 pb-40">
		<div class="col-sm-6">
			<img src="<?= base_url('assets/images/cowork.jpg') ?>" class="img-responsive">
		</div>
		<div class="col-sm-6">
			<h3 class="mt-0">Co-working Spaces in UAE</h3>
			<p class="text-justify">We offer shared co-working space with over 100 companies and startups in residence. Our co-working space is home to many established organisationas and equally supporting start-up ecosystem. We provide state-of-the-art coworking facilities in a collaborative and friendly community.</p>
		</div>
	</div>
</div>